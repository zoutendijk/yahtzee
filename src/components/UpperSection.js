import React from "react";
import Singles from "./scores/Singles";

function LowerSection({ rolls, totalUpperScore, updateUpperScore }) {
  return (
    <div className="scoresheet__column">
      <Singles
        rolls={rolls}
        updateScore={score => updateUpperScore(score)}
        title="Ones"
        number={1}
      />
      <Singles
        rolls={rolls}
        updateScore={score => updateUpperScore(score)}
        title="Twos"
        number={2}
      />
      <Singles
        rolls={rolls}
        updateScore={score => updateUpperScore(score)}
        title="Threes"
        number={3}
      />
      <Singles
        rolls={rolls}
        updateScore={score => updateUpperScore(score)}
        title="Fours"
        number={4}
      />
      <Singles
        rolls={rolls}
        updateScore={score => updateUpperScore(score)}
        title="Fives"
        number={5}
      />
      <Singles
        rolls={rolls}
        updateScore={score => updateUpperScore(score)}
        title="Sixes"
        number={6}
      />

      <div className="scoresheet__total-score">
        Total score (upper section): <strong>{totalUpperScore}</strong>
      </div>
      <div className="scoresheet__total-score">
        Bonus (>= 63): <strong>{totalUpperScore >= 63 ? 35 : 0}</strong>
      </div>
      <div className="scoresheet__total-score">
        Total score (with bonus):
        <strong>{(totalUpperScore >= 63 ? 35 : 0) + totalUpperScore}</strong>
      </div>
    </div>
  );
}

export default LowerSection;
