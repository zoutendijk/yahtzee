import React, { useState } from "react";
import LowerSection from "./LowerSection";
import UpperSection from "./UpperSection";

function Scoresheet({ diceMeta, nextTurn }) {
  const rolls = diceMeta.map(die => die.roll);
  const [totalLowerScore, updateTotalLowerScore] = useState(0);
  const [totalUpperScore, updateTotalUpperScore] = useState(0);

  const updateLowerScore = score => {
    updateTotalLowerScore(oldScore => oldScore + score);
    nextTurn();
  };

  const updateUpperScore = score => {
    updateTotalUpperScore(oldScore => oldScore + score);
    nextTurn();
  };

  return (
    <div className="scoresheet">
      <UpperSection
        rolls={rolls}
        totalUpperScore={totalUpperScore}
        updateUpperScore={score => updateUpperScore(score)}
      />
      <LowerSection
        rolls={rolls}
        totalLowerScore={totalLowerScore}
        updateLowerScore={score => updateLowerScore(score)}
      />
      <div className="scoresheet__grand-total">
        Grand total:
        <strong>
          {(totalUpperScore >= 63 ? 35 : 0) + totalLowerScore + totalUpperScore}
        </strong>
      </div>
    </div>
  );
}

export default Scoresheet;
