import React from "react";

function Die({ locked, roll, onClick }) {
  return (
    <div
      onClick={onClick}
      className={`die die--${roll} ${locked ? "is-locked" : ""}`}
    />
  );
}

export default Die;
