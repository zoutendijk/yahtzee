import React, { useState } from "react";
import "../css/App.css";
import Dice from "./Dice";
import Scoresheet from "./Scoresheet";
import { GameProvider } from "./GameContext";

function randomNumber() {
  return Math.round(Math.random() * 5) + 1;
}

function Game() {
  const createEmptyDiceMeta = () => {
    const dice = [];
    for (let i = 0; i < 5; i++) {
      const element = {
        id: i,
        roll: 0,
        locked: false
      };
      dice.push(element);
    }
    return dice;
  };

  const [diceMeta, updateDiceMeta] = useState(createEmptyDiceMeta());
  const [rollCount, updateRollCount] = useState(0);

  const setLocked = id => {
    if (diceMeta[id].roll !== 0) {
      const newDiceMeta = diceMeta;
      newDiceMeta[id].locked = !diceMeta[id].locked;
      updateDiceMeta([...newDiceMeta]); // Create new reference to force rerender
    }
  };

  const rollDice = () => {
    const newDiceMeta = diceMeta.length ? diceMeta : createEmptyDiceMeta();
    for (let i = 0; i < newDiceMeta.length; i++) {
      if (!newDiceMeta[i].locked) {
        newDiceMeta[i].roll = randomNumber();
      }
    }
    updateRollCount(roll => roll + 1);
    updateDiceMeta([...newDiceMeta]);
  };

  const nextTurn = () => {
    updateRollCount(0);
    updateDiceMeta(createEmptyDiceMeta());
  };

  return (
    <GameProvider value={rollCount}>
      <div className="container">
        <Scoresheet diceMeta={diceMeta} nextTurn={() => nextTurn()} />
        <Dice diceMeta={diceMeta} onClick={id => setLocked(id)} />
        <button
          className="button button--2"
          onClick={rollDice}
          disabled={rollCount >= 3}
        >
          {rollCount <= 2
            ? `Roll dice (${rollCount + 1} / 3)`
            : "Make a choice"}
        </button>
      </div>
    </GameProvider>
  );
}

export default Game;
