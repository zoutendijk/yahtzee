import React from "react";
import Die from "./Die";

function Dice({ diceMeta, onClick }) {
  const dice = [];
  for (let x = 0; x < diceMeta.length; x++) {
    dice.push(
      <Die
        key={diceMeta[x].id}
        roll={diceMeta[x].roll}
        locked={diceMeta[x].locked}
        onClick={() => onClick(diceMeta[x].id)}
      />
    );
  }
  return <div className="dice">{dice}</div>;
}

export default Dice;
