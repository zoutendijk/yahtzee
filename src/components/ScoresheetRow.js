import React, { useState, useContext } from "react";
import GameContext from "./GameContext";

function ScoresheetRow({ title, score, onClick }) {
  const [finalScore, updateFinalScore] = useState(-1);
  const [used, updateUsed] = useState(false);
  const rollCount = useContext(GameContext);

  return (
    <div className={`scoresheet__row ${used ? "is-used" : ""}`}>
      <div className="scoresheet__title">{title}</div>
      <div className="scoresheet__score">
        {finalScore !== -1 ? finalScore : score}
      </div>
      <button
        disabled={used || rollCount === 0}
        onClick={() => {
          onClick(score);
          updateFinalScore(score);
          updateUsed(true);
        }}
        className="button scoresheet__button"
      >
        Use
      </button>
    </div>
  );
}

export default ScoresheetRow;
