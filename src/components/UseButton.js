import React, { useState } from "react";

function UseButton({ onClick, score }) {
  const [used, updateUsed] = useState(false);

  return (
    <button
      disabled={used}
      onClick={() => {
        onClick();
        updateUsed(true);
      }}
      className="scoresheet__button"
    >
      Use
    </button>
  );
}

export default UseButton;
