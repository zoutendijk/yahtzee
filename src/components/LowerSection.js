import React from "react";
import ThreeOfAKind from "./scores/ThreeOfAKind";
import FourOfAKind from "./scores/FourOfAKind";
import Topscore from "./scores/Topscore";
import FullHouse from "./scores/FullHouse";
import SmallStraight from "./scores/SmallStraight";
import LargeStraight from "./scores/LargeStraight";
import Chance from "./scores/Chance";

function LowerSection({ rolls, totalLowerScore, updateLowerScore }) {
  return (
    <div className="scoresheet__column">
      <ThreeOfAKind
        rolls={rolls}
        updateScore={score => updateLowerScore(score)}
      />
      <FourOfAKind
        rolls={rolls}
        updateScore={score => updateLowerScore(score)}
      />
      <FullHouse rolls={rolls} updateScore={score => updateLowerScore(score)} />
      <SmallStraight
        rolls={rolls}
        updateScore={score => updateLowerScore(score)}
      />
      <LargeStraight
        rolls={rolls}
        updateScore={score => updateLowerScore(score)}
      />
      <Topscore rolls={rolls} updateScore={score => updateLowerScore(score)} />
      <Chance rolls={rolls} updateScore={score => updateLowerScore(score)} />
      <div className="scoresheet__total-score">
        Total score (lower section): <strong>{totalLowerScore}</strong>
      </div>
    </div>
  );
}

export default LowerSection;
