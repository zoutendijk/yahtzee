import React from "react";
import ScoresheetRow from "../ScoresheetRow";

function ThreeOfAKind({ rolls, updateScore }) {
  let result = [];

  for (let i = 0; i < rolls.length; i++) {
    const number = rolls[i];

    for (let j = 0; j < rolls.length; j++) {
      const number2 = rolls[j];

      if (number === number2) {
        result.push(number);
      }
    }
    if (result.length >= 3) {
      break;
    } else {
      result = [];
    }
  }

  let score = result.length >= 3 ? rolls.reduce((a, b) => a + b) : 0;
  return (
    <ScoresheetRow
      title="Three of a kind"
      score={score}
      onClick={() => updateScore(score)}
    />
  );
}

export default ThreeOfAKind;
