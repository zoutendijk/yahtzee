import React from "react";
import ScoresheetRow from "../ScoresheetRow";

function Topscore({ rolls, updateScore }) {
  let result = true;

  for (let i = 0; i < rolls.length; i++) {
    if (rolls[i] !== rolls[0] || rolls[i] === 0) {
      result = false;
      break;
    }
  }

  let score = result ? 50 : 0;
  return (
    <ScoresheetRow
      title="Topscore"
      score={score}
      onClick={() => updateScore(score)}
    />
  );
}

export default Topscore;
