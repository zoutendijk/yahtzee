import React from "react";
import ScoresheetRow from "../ScoresheetRow";

function FullHouse({ rolls, updateScore }) {
  let sortedRolls = rolls.sort();
  let result = false;

  if (sortedRolls[0] === sortedRolls[1]) {
    if (
      sortedRolls[1] === sortedRolls[2] &&
      sortedRolls[2] !== sortedRolls[3]
    ) {
      if (sortedRolls[3] === sortedRolls[4]) {
        result = true;
      }
    }
    if (
      sortedRolls[1] !== sortedRolls[2] &&
      sortedRolls[2] === sortedRolls[3] &&
      sortedRolls[2] === sortedRolls[4]
    ) {
      result = true;
    }
  }

  let score = result ? 25 : 0;

  return (
    <ScoresheetRow
      title="Full House"
      score={score}
      onClick={() => updateScore(score)}
    />
  );
}

export default FullHouse;
