import React from "react";
import ScoresheetRow from "../ScoresheetRow";

function Singles({ rolls, updateScore, number, title }) {
  let hit = 0;

  for (let i = 0; i < rolls.length; i++) {
    const roll = rolls[i];
    if (roll === number) {
      hit++;
    }
  }
  let score = hit * number;
  return (
    <ScoresheetRow
      title={title}
      score={score}
      onClick={() => updateScore(score)}
    />
  );
}

export default Singles;
