import React from "react";
import ScoresheetRow from "../ScoresheetRow";

function Chance({ rolls, updateScore }) {
  let score = rolls.reduce((a, b) => a + b);

  return (
    <ScoresheetRow
      title="Chance"
      score={score}
      onClick={() => updateScore(score)}
    />
  );
}

export default Chance;
