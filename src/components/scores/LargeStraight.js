import React from "react";
import ScoresheetRow from "../ScoresheetRow";

function LargeStraight({ rolls, updateScore }) {
  let sortedRolls = rolls.sort();
  let hits = 1;

  for (let i = 0; i < sortedRolls.length; i++) {
    if (
      i < sortedRolls.length - 1 &&
      sortedRolls[i] === sortedRolls[i + 1] - 1
    ) {
      hits++;
    }
  }

  let score = hits >= 5 ? 40 : 0;

  return (
    <ScoresheetRow
      title="Large Straight"
      score={score}
      onClick={() => updateScore(score)}
    />
  );
}

export default LargeStraight;
