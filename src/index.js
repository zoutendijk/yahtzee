import React from "react";
import ReactDOM from "react-dom";
import Game from "./components/Game";
import "normalize.css";

ReactDOM.render(<Game />, document.getElementById("root"));
